<?php
	
/**
 * include from some source
 */
require_once 'config/sample-domain.php';	
	
/**
 * or paste data here
 */
	
$config = array(
	//token master
	'1234567890abc' => array(
		'root' => '/path/to/site/www/',
		'log' => '/path/to/branch-master/log/file/log.log',
		'branch' => 'master',
        'command' => ''
		
	),
	//token dev
	'1234567890xyz' => array(
		'root' => '/path/to/site/dev/',
		'log' => '/path/to/branch-dev/log/file/log.log',
		'branch' => 'dev',
        'command' => ''
	),
);

