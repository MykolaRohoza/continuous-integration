<?php 
	header('Powered: on-Perdil`noTyaglovostyah'); 
	header('Content-Type: text/html; charset=utf-8');
	$config_path = realpath('../../config.php');
	$error_message = '';
	
	if(is_file($config_path)){
		require_once $config_path;
	} else {
		addErrorMessage("No configs file");
	}
	if($config){

		if(checkConfigs($config)){
			
		} else {
			
			addErrorMessage("Wrong configs");
			die($error_message);
		}
	} else {
		addErrorMessage("No configs");
		die($error_message);
	}

	
	foreach ($config as $config_item){
		echo '<H1>' . $config_item['branch'] . ' - '  . basename($config_item['root']) . '</H1>';
		echo nl2br(file_get_contents($config_item['log']));
	}
	
	function checkConfigs($config)
	{
		$result = true;
		foreach ($config as $config_item) {
			$tmp_result = checkConfig($config_item);
			if($result && !$tmp_result)
			{
				$result = false;
			}
		}
		return $result;
	}
	function checkConfig($config_item)
	{
		$result = true;
		if(!is_dir($config_item['root'])){
			addErrorMessage($config_item['root'] . ' is not directory' );
			return false;
		}
		if(!is_dir($config_item['root'] . '/.git')){
			addErrorMessage($config_item['root'] . ' has no git' );
			return false;
		}
		$actual_branch = exec('LOCALE=C LC_ALL=C && cd ' . $config_item['root'] . ' && ' . 'git branch | grep "*"');
		if($actual_branch != ('* ' . $config_item['branch'])){
			addErrorMessage('actual_branch:' . $actual_branch . ', dir on branch: ' . $config_item['branch'] );
			return false;
		}
		$log_dirs = explode('/', $config_item['log']);
		unset($log_dirs[count($log_dirs) -1]);
		$log_dir = '';
		foreach($log_dirs as $log_dirs_item){
			$log_dir .= '/' . $log_dirs_item;
			if(!is_dir($log_dir)){
				addErrorMessage($log_dir . ' is not dir');
				$result = false;
			}
		}

		
		if(!is_file($config_item['log'])){
			$res = fopen($config_item['log'], 'w');
			fwrite($res, '');
			fclose($res);
		}
		if(!is_file($config_item['log']) || !is_writable($config_item['log'])){
			addErrorMessage($config_item['log'] . ' is not writable or does not exists');
			$result = false;
		}
		// to do check is it 
		
		return $result;
	}
	
	function addErrorMessage($message)
	{
		global $error_message;
		$error_message .= '<H1>' . $message . '</H1>';
	}
	echo $error_message;